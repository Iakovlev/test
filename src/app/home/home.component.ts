import {
  Component,
  OnInit
} from '@angular/core';

import { AppState } from '../app.service';
import { Title } from './title';
import { XLargeDirective } from './x-large';

@Component({
  /**
   * The selector is what angular internally uses
   * for `document.querySelectorAll(selector)` in our index.html
   * where, in this case, selector is the string 'home'.
   */
  selector: 'home',  // <home></home>
  /**
   * We need to tell Angular's Dependency Injection which providers are in our app.
   */
  providers: [
    Title
  ],
  /**
   * Our list of styles in our component. We may add more to compose many styles together.
   */
  styleUrls: [ './home.component.css' ],
  /**
   * Every Angular template is first compiled by the browser before Angular runs it's compiler.
   */
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
  /**
   * Set our default values
   */
  public localState = { value: '' };
  /**
   * TypeScript public modifiers
   */
  constructor(
    public appState: AppState,
    public title: Title
  ) {}

  public ngOnInit() {
    console.log('hello `Home` component');
    /**
     * this.title.getData().subscribe(data => this.data = data);
     */
  }

  public asyncErrorPlease(one, two, three) {
    if(one==1){
      console.log("one is one");
      setTimeout(()=>{
        console.log("inside setTimeout");
        throw new Error("one was one and two and three were: " + two + three);
      }, 1000);
    }else{
      console.log("one is not one");
      throw new Error("one was not one");
    }
  }

  public errorPlease(one, two, three) {
    if(one==1){
      console.log("one is one");
      setTimeout(()=>{
        console.log("inside setTimeout");
        throw new Error("one was one and two and three were: " + two + three);
      }, 1000);
    }else{
      console.log("one is not one");
      throw new Error("one was not one");
    }
  }

  public submitState(value: string) {
    console.log('submitState', value);
    this.appState.set('value', value);
    this.localState.value = '';
    console.log("some changes here");
    var a: any;
    var t = a.test;
  }
}
